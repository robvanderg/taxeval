import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

def getScore(path):
    scores = []
    totalScore = 0.0
    for seed in range(1,11):
        for line in open(path.replace('SEED', str(seed))):
        #for line in open(path.replace('SEED.', '')):
            if line.startswith('LAS'):
                scores.append(float(line.split(' ')[-1]))
                totalScore += float(line.split(' ')[-1])
    return totalScore/len(scores)

def misc2dict(misc):
    result = {}
    for item in misc.strip().split('|'):
        if item == '_':
            continue
        split = item.split('=')
        result[split[0]] = '='.join(split[1:])
    return result

def getNormScore(conlData, cat):
    total = 0
    cor = 0
    for line in open(conlData):
        tok = line.strip().split('\t')
        if len(tok) < 5:
            continue
        misc = misc2dict(tok[-1])
        if str(misc['NormCat']) == str(cat):
            total += 1
            if misc['Norm'] == misc['PredNorm']:
                cor += 1
    if total == 0:
        return 0
    return cor/total

def getFreq(conlData, cat):
    total = 0
    for line in open(conlData):
        tok = line.strip().split('\t')
        if len(tok) < 5:
            continue
        misc = misc2dict(tok[-1])
        if str(misc['NormCat']) == str(cat):
            total += 1
    return total


def makeGraph(setting):
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    base = getScore('preds/owoputi.base.SEED.eval')
    upper = getScore('preds/owoputi.' + setting + '.upper.SEED.eval')
    print(base, upper)

    freqs = []
    scores = []
    normScores = []
    for cat in range(1,16):
        iso = getScore('preds/owoputi.iso.SEED.' + setting + '.' + str(cat) + '.eval') - base
        abl = upper - getScore('preds/owoputi.abl.SEED.' + setting + '.' + str(cat) + '.eval')
        scores.append([iso, abl])
        norm = getNormScore('data/owoputi.all', cat)
        normScores.append(norm)
        freqs.append(getFreq('data/owoputi.all', cat))
    names = ['Typo', 'Missing apo.', 'Spelling err.', 'Split', 'Merge', 'Phrasal abbr.', 'Repetition', 'Short. vow.', 'Short. end', 'Short. other', 'Reg. trans', 'Other trans.', 'Slang', 'Unk', 'Inf. Contract.']

    #swap order of last two categories, to visualize unk as latest
    tmpName = names[-2]
    tmpScore = scores[-2]
    tmpNorm = normScores[-2]

    names[-2] = names[-1]
    scores[-2] = scores[-1]
    normScores[-2] = normScores[-1]

    names[-1] = tmpName
    scores[-1] = tmpScore
    normScores[-1] = tmpNorm

    # add normalization which were not annotated as category
    if setting == 'pred':
        iso = getScore('preds/owoputi.iso.SEED.' + setting + '.rest.eval') - base
        abl = upper - getScore('preds/owoputi.abl.SEED.' + setting + '.rest.eval')
        scores.append([iso, abl])
        normScores.append(0.0)
        names.append('other')

    #if setting == 'pred':
    #    ax.plot([0,16], [upper-base, upper-base], color='black', label='All', linestyle='--', linewidth=1)
    #ax.plot([-2],[0], 0.5, color='black', label='Normalization rec.' if setting=='pred' else 'Frequency')


    myutils.drawGraph(ax, scores, ['Isolation', 'Ablation'], names, '','', (-.5,2), '')
    if setting == 'pred':
        leg = ax.legend(loc='upper left', prop={'size': 13})
    if setting == 'gold':
        leg = ax.legend(loc='upper left', prop={'size': 13})
    leg.get_frame().set_linewidth(1)

    plt.ylabel('Difference in LAS')
    if setting == 'pred':
        ax2 = ax.twinx()
        idxs = []
        for i in range(len(normScores)):
            idxs.append(i + .5)
        ax2.grid(False)

        ax2.bar(idxs, normScores, .05, color='black')
        ax2.set_ylabel('Normalization Recall')
        ax2.set_ylim(-.2,.8)
        ax2.set_yticks([0.0,0.2,0.4,0.6,0.8])
        ax2.set_xlim(0,len(normScores))
        ax2.plot([-2,-1], [0,0], color='black', label='Norm.\nRec.')
        leg = ax2.legend(loc='upper right', prop={'size':13})
        leg.get_frame().set_linewidth(1)
    #if setting == 'gold':    
        #ax2 = ax.twinx()
        #idxs = []
        #for i in range(len(freqs)):
        #    idxs.append(i + .5)
        #ax2.grid(False)
        #ax2.bar(idxs, freqs, .05, color='black')
        #ax2.set_ylabel('Frequency')
        #ax2.set_ylim(-25,200)
        #ax2.set_yticks([0,50,100,150,200])
        #ax2.set_xlim(0, len(freqs))
        #ax2.plot([-2,-1], [0,0], color='black', label='Freq.')
        #leg = ax2.legend(loc='upper left', prop={'size':13}, bbox_to_anchor=(.79, 0.98))
        #leg.get_frame().set_linewidth(1)
            
    plt.savefig(setting + '.pdf', bbox_inches='tight')

makeGraph('gold')
makeGraph('pred')

