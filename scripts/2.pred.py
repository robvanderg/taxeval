


for seed in range(1,11):
    for cat in range(0,16):
        for mode in ['pred', 'gold']:
            for eval in ['abl', 'iso']:
                testFile = '../splits/owoputi.all.' + eval + '.' + mode + '.' + str(cat)
                outFile = '.'.join(['owoputi',eval,str(seed),mode,str(cat)])
                cmd = 'cd normpar'
                cmd += ' && python3 src/parser.py --tweets --pred ' + testFile + ' --model ../data/parserModel.' + str(seed) + '/model --params ../data/parserModel.' + str(seed) + '/params.pickle --out ' + outFile
                cmd += ' --seed ' + str(seed)
                cmd += ' && cp ' + outFile + ' ../preds/'
                cmd += ' && cd ../'
                print(cmd)


    for eval in ['abl', 'iso']:
        mode = 'pred'
        cat = 'rest'
        testFile = '../splits/owoputi.all.' + eval + '.' + mode + '.' + str(cat)
        outFile = '.'.join(['owoputi',eval,str(seed),mode,str(cat)])
        cmd = 'cd normpar'
        cmd += ' && python3 src/parser.py --tweets --pred ' + testFile + ' --model ../data/parserModel.' + str(seed) + '/model --params ../data/parserModel.' + str(seed) + '/params.pickle --out ' + outFile
        cmd += ' --seed ' + str(seed)
        cmd += ' && cp ' + outFile + ' ../preds/'
        cmd += ' && cd ../'
        print(cmd)

    # base
    cat = 'base'
    testFile = '../splits/owoputi.all.base'
    outFile = 'owoputi.base.' + str(seed)
    cmd = 'cd normpar'
    cmd += ' && python3 src/parser.py --tweets --pred ' + testFile + ' --model ../data/parserModel.' + str(seed) + '/model --params ../data/parserModel.' + str(seed) + '/params.pickle --out ' + outFile
    cmd += ' --seed ' + str(seed)
    cmd += ' && cp ' + outFile + ' ../preds/'
    cmd += ' && cd ../'
    print(cmd)

    # upper bounds (activate all categories)
    for mode in ['pred', 'gold']:
        testFile = '../splits/owoputi.all.' + mode + '.upper'
        outFile = 'owoputi.' + mode + '.upper.' + str(seed)
        cmd = 'cd normpar'
        cmd += ' && python3 src/parser.py --tweets --pred ' + testFile + ' --model ../data/parserModel.' + str(seed) + '/model --params ../data/parserModel.' + str(seed) + '/params.pickle --out ' + outFile
        cmd += ' --seed ' + str(seed)
        cmd += ' && cp ' + outFile + ' ../preds/'
        cmd += ' && cd ../'
        print(cmd)
