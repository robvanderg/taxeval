

for i in range(1,11):
    cmd = 'cd normpar && python3 src/parser.py --train UD_English-EWT/en_ewt-ud-train.conllu --dev UD_English-EWT/en_ewt-ud-dev.conllu '
    cmd += '--usehead --userlmost --tweets --out ../data/parserModel.' + str(i) + ' --seed ' + str(i) 
    cmd += ' &> out.' + str(i) + ' && cd .. '
    print(cmd)
