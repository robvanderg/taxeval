#TODO, this file is way to redundant
import sys
import os

if len(sys.argv) < 2:
    print("please provide conllu file with Norm, NormCat and PredNorm in the misc column")
    exit(1)

if not os.path.exists('splits'):
    os.mkdir('splits')

def misc2dict(misc):
    result = {}
    for item in misc.split('|'):
        if item == '_':
            continue
        split = item.split('=')
        result[split[0]] = '='.join(split[1:])
    return result

for cat in range(0,16):
    for mode in ['pred', 'gold']:
        for eval in ['abl', 'iso']:
            outFile = open('splits/' + '.'.join([sys.argv[1].split('/')[-1], eval, mode, str(cat)]), 'w')
            for line in open(sys.argv[1]):
                tok = line.strip().split('\t')
                if len(tok) < 5:
                    outFile.write(line)
                else:
                    misc = misc2dict(tok[-1])
                    if eval == 'iso' and misc['NormCat'] == str(cat) and cat != '0':
                        tok[1] = misc['PredNorm'] if mode == 'pred' else misc['Norm']
                    if eval == 'abl' and misc['NormCat'] != str(cat) and cat != '0':
                        tok[1] = misc['PredNorm'] if mode == 'pred' else misc['Norm']
                    outFile.write('\t'.join(tok) + '\n')
            outFile.close()

# generate 'rest' files
for eval in ['abl', 'iso']:
    outFile = open('splits/' + '.'.join([sys.argv[1].split('/')[-1], eval, 'pred', 'rest']), 'w') 
    for line in open(sys.argv[1]):
        tok = line.strip().split('\t')
        if len(tok) < 5:
            outFile.write(line)
        else:
            misc = misc2dict(tok[-1])
            #if misc['NormCat'] == '0' and misc['PredNorm'] != tok[1]:
            #    print(misc['PredNorm'], tok[1])
            if eval == 'iso' and misc['NormCat'] == '0':
                tok[1] = misc['PredNorm']
            if eval == 'abl' and misc['NormCat'] != '0':
                tok[1] = misc['PredNorm']
            outFile.write('\t'.join(tok) + '\n')
    outFile.close()

# generate base
os.system('cp ' + sys.argv[1] + ' splits/owoputi.all.base')

# generate upperbound pred
outFile = open('splits/' + '.'.join([sys.argv[1].split('/')[-1], 'pred', 'upper']), 'w')
for line in open(sys.argv[1]):
    tok = line.strip().split('\t')
    if len(tok) < 5:
        outFile.write(line)
    else:
        misc = misc2dict(tok[-1])
        #if misc['NormCat'] == '0' and misc['PredNorm'] != tok[1]:
        #    print(misc['PredNorm'], tok[1])
        tok[1] = misc['PredNorm']
        outFile.write('\t'.join(tok) + '\n')
outFile.close()
    
 
# generate upperbound gold
outFile = open('splits/' + '.'.join([sys.argv[1].split('/')[-1], 'gold', 'upper']), 'w')
for line in open(sys.argv[1]):
    tok = line.strip().split('\t')
    if len(tok) < 5:
        outFile.write(line)
    else:
        misc = misc2dict(tok[-1])
        #if misc['NormCat'] == '0' and misc['PredNorm'] != tok[1]:
        #    print(misc['PredNorm'], tok[1])
        tok[1] = misc['Norm']
        outFile.write('\t'.join(tok) + '\n')
outFile.close()

