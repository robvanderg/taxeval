import sys

import sys

if len(sys.argv) < 5:
    print('please specify conll file, annotation file, normalization file and out file')
    exit(1)

def getConlData(path):
    data = []
    curSent = []
    for line in open(path):
        line = line.strip()
        tok = line.strip().split('\t')
        if len(line) < 1:
            data.append(curSent)
            curSent = []
        else:
            if '-' in tok[0] and len(tok) == 10:
                continue
            curSent.append(line.split('\t'))
    return data

#def getNormData(path):
#    data = []
#    for line in open(path):
#        
#    return data


conlData = getConlData(sys.argv[1])
annData = getConlData(sys.argv[2])
normData = getConlData(sys.argv[3])

if len(conlData) != len(annData) or len(annData) != len(normData):
    print('len(conlData) = ', len(conlData), 'len(annData) = ', len(annData), 'len(normData) = ', len(normData))

outFile = open(sys.argv[4], 'w')
for i in range(len(conlData)):
    conl = conlData[i]
    ann = annData[i]
    norm = normData[i]
    annIdx = 0
    cat = -1
    for conlIdx in range(len(conl)):
        if conl[conlIdx][0][0] == '#':
            outFile.write(conl[conlIdx][0] + '\n')
        else:
            if cat != -1:
                conl[conlIdx][-1] += '|NormCat=' + str(cat)
                conl[conlIdx][-1] += '|PredNorm=' + norm[int(conl[conlIdx][0])-1][0]
                outFile.write('\t'.join(conl[conlIdx]) + '\n')
                cat = -1
                annIdx += 1
            elif ann[annIdx][1] == conl[conlIdx][1]:
                conl[conlIdx][-1] += '|NormCat=' + str(ann[annIdx][0])
                conl[conlIdx][-1] += '|PredNorm=' + norm[int(conl[conlIdx][0])-1][0]
                outFile.write('\t'.join(conl[conlIdx]) + '\n')
                annIdx += 1
            else:
                if conl[conlIdx][1] + conl[conlIdx+1][1] == ann[annIdx][1]:
                    cat = ann[annIdx][0]
                    conl[conlIdx][-1] += '|NormCat=' + str(ann[annIdx][0])
                    conl[conlIdx][-1] += '|PredNorm=' + norm[int(conl[conlIdx][0])-1][0]
                    outFile.write('\t'.join(conl[conlIdx]) + '\n')
                else:
                    print('NO', annIdx, conlIdx)
                    for item in conl:
                        print(item)
                    print()
                    for item in ann:
                        print(item)
    outFile.write('\n')

outFile.close()

