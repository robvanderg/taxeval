def misc2dict(misc):
    result = {}
    for item in misc.split('|'):
        if item == '_':
            continue
        split = item.split('=')
        result[split[0]] = '='.join(split[1:])
    return result



freqs = {}
total = 0
for line in open('data/owoputi.all'):
    tok = line.strip().split()
    if len(tok) == 0 or tok[0][0] == '#':
        continue
    cat = int(misc2dict(tok[-1])['NormCat'])
    if cat not in freqs:
        freqs[cat] = 0
    freqs[cat] += 1
    total += 1

names = ['None', 'Typo', 'Missing apo.', 'Spelling err.', 'Split', 'Merge', 'Phrasal abbr.', 'Repetition', 'Short. vow.', 'Short. end', 'Short. other', 'Reg. trans', 'Other trans.', 'Slang', 'Unk', 'Inf. Contract.']

for cat in sorted(freqs):
    print(names[cat], '&', freqs[cat], '&', '{:.2f}'.format(100* (freqs[cat]/total)), '\\\\')


