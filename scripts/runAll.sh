sudo apt install icmake
pip3 install --user dynet

mkdir -p data
# Get annotation for normalization categories
git clone https://github.com/wesselreijngoud/masterthesis2019
cat masterthesis2019/Data/LexNorm/lexnorm.train masterthesis2019/Data/LexNorm/lexnorm.dev masterthesis2019/Data/LexNorm/lexnorm.test > data/lexnorm.tax.gold
cat masterthesis2019/Data/Owoputi/owoputi.final.train2 masterthesis2019/Data/Owoputi/owoputi.final.dev masterthesis2019/Data/Owoputi/owoputi.final.test > data/owoputi.tax.gold
# `patch' owoputi data
sed -i '/^$/{n;/^$/d}' data/owoputi.tax.gold

# Get treebank annotation
git clone https://robvanderg@bitbucket.org/robvanderg/normpar.git
cp normpar/data/owoputi.conllu data
cp normpar/data/lexnorm.conllu data
grep -v "^[0-9]*-" data/owoputi.conllu | grep -v "^#" | cut -f 2 > data/owoputi.raw
grep -v "^[0-9]*-" data/lexnorm.conllu | grep -v "^#" | cut -f 2 > data/lexnorm.raw

# Get normalization predictions
git clone https://robvanderg@bitbucket.org/robvanderg/monoise.git
cd monoise/
mkdir data
cd data
curl www.robvandergoot.com/data/monoise/en.tar.gz | tar xvz
cd ../src
icmbuild
./tmp/bin/binary -m RU -i ../../data/owoputi.raw -W -C -S -d ../data/en -r ../data/en/liliu.model > ../../data/owoputi.pred
./tmp/bin/binary -m RU -i ../../data/lexnorm.raw -W -C -S -d ../data/en -r ../data/en/liliu.model > ../../data/lexnorm.pred
cd ../../

# Merge all annotations
python3 scripts/0.mergeBack.py data/owoputi.conllu data/owoputi.tax.gold data/owoputi.pred data/owoputi.all
python3 scripts/0.mergeBack.py data/lexnorm.conllu data/lexnorm.tax.gold data/lexnorm.pred data/lexnorm.all

# Split the annotations, each split in the splits folder will have annotation for only one category
python3 scripts/1.makeSets.py data/owoputi.all

# Train the parser
cd normpar
curl https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2988/ud-treebanks-v2.4.tgz | tar xvz
mv ud-treebanks-v2.4/UD_English-EWT .
rm -rf ud-treebanks-v2.4*
cd ..
python3 scripts/2.train.py > run.sh
./run.sh

# Run the parser
mkdir -p preds
python3 scripts/2.pred.py > run.sh
chmod +x run.sh
./run.sh

# Evaluate the parser
python3 scripts/2.replWordsInConl.py
python3 scripts/2.eval.py

