import os

gold = 'data/owoputi.all'

for path in os.listdir('preds'):
    if path.endswith('.eval'):
        continue
    pred = 'preds/' + path
    out = pred + '.eval'
    cmd = 'python3 scripts/conll18_ud_eval.py ' + gold + ' ' + pred + ' > ' + out
    os.system(cmd)
    #print(cmd)

