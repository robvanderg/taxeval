import os

gold = open('data/owoputi.all').readlines()

for path in os.listdir('preds'):
    if path.endswith('.eval'):
        continue
    new = []
    for goldLine, predLine in zip(gold, open('preds/' + path)):
        predTok = predLine.strip().split('\t')
        if len(predTok) < 2:
            new.append(predTok)
        else:
            predTok[1] = goldLine.split('\t')[1]
            new.append(predTok)
    outFile = open('preds/' + path, 'w')
    for item in new:
        outFile.write('\t'.join(item) + '\n')
    outFile.close()

