import matplotlib.pyplot as plt
import matplotlib as mpl
from copy import deepcopy
import os
import sys

plt.style.use('scripts/rob.mplstyle')
if sys.version_info[1] < 6:
    colors = [x['color'] for x in list(plt.rcParams["axes.prop_cycle"])]
else:
    colors = plt.rcParams["axes.prop_cycle"].by_key()["color"] 
colors = colors + colors

def setTicks(ax, labels, rotation = 0):
    ticks = []
    for i in range (len(labels)):
        ticks.append(i + .675)

    ax.xaxis.set_major_locator(mpl.ticker.LinearLocator(len(labels)+1))
    ax.xaxis.set_minor_locator(mpl.ticker.FixedLocator(ticks))

    ax.xaxis.set_major_formatter(mpl.ticker.NullFormatter())
    ax.xaxis.set_minor_formatter(mpl.ticker.FixedFormatter(labels))

    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)
        tick.label1.set_horizontalalignment('right')
        tick.label1.set_rotation(rotation)

def drawGraph(ax,allScores, subDivision, mainDivision, xlabel, ylabel, ylim, location='best'):
    if len(allScores) != len(mainDivision):
        print("error, different size of scores and main labels:", len(allScores), len(mainDivision))
    if len(allScores[0]) != len(subDivision):
        print("error, different size of scores and sub labels:", len(allScores), len(mainDivision))
    print(mainDivision)

    bar_width = 1/ (len(subDivision) + 1)

    for subIdx in range(len(subDivision)):
        index = []
        scores = []
        for mainIdx in range(len(mainDivision)):
            index.append(mainIdx + (subIdx + 1) * bar_width)
            scores.append(allScores[mainIdx][subIdx])

        print(subDivision[subIdx], scores)
        ax.bar(index, scores, bar_width, label=subDivision[subIdx], color=colors[subIdx])
    print()

    setTicks(ax, mainDivision, 45)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim((0,len(mainDivision)))
    ax.set_ylim(ylim)

    if location != '':
        leg = ax.legend(loc=location, prop={'size': 15})
        leg.get_frame().set_linewidth(1.5)

