### An In-depth Analysis of the Effect of Lexical Normalization on theDependency Parsing of Social Media ###

This repository contains the following folders:

* data: contains all the annotations, normalization predictions and the parser model. If you are here for the data, owoputi.all and lexnorm.all are probably the files you are looking for (these contain all layers). ps. the LexNorm data (test) is not used in the paper, as it is an analysis paper.
* preds: predictions and scores of the parser on the development (owoputi) data.
* scripts: the scripts used to obtain all results, see runAll.sh how all the results are obtained (however, re-running is not necessary, as all predictions are saved in this repository). You can use genAll.sh to generate the table and graph in the paper
* splits: versions of the development data where only one normalization category is annotated.

Actually, all you would need to reproduce the results/datasets from the paper is the scipts directory; move this to another path and then run runAll.sh to obtain the rest of this repository.

### Categories ###
For datasets with no normalization, the files \*.iso.gold.0 can be used. In the annotation the 0 category is used for words which are not normalized. The numbers of the other categories are as follows:

1. Typo 
2. Missing apostrophe
3. Spelling error
4. Split
5. Merge
6. Phrasal abbreviation
7. Repetition
8. Shortening vowel
9. Shortening end
10. Shortening other
11. Regular transformation
12. Other transformation
13. Slang
14. Unk
15. Informal. Contractions

Note that in scripts/3.graph.py I swapped the order of the last two categories (because it is nicer to visualize unk as final category)

### paper ###
TBA

